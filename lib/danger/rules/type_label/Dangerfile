# frozen_string_literal: true

TYPE_LABEL_MISSING_MESSAGE = "Please add a [merge request type](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) to this merge request."

require_relative "../../../gitlab/dangerfiles/type_label_guesser"

return if helper.has_scoped_label_with_scope?("type")

if respond_to?(:changelog)
  type_label_guesser = Gitlab::Dangerfiles::TypeLabelGuesser.new
  helper.labels_to_add.concat(type_label_guesser.labels_from_changelog_categories(changelog.categories))
end

if ENV['DANGER_ERROR_WHEN_TYPE_LABEL_IS_MISSING'] == 'true'
  fail TYPE_LABEL_MISSING_MESSAGE
else
  warn TYPE_LABEL_MISSING_MESSAGE
end
